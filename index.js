const http = require('http');
const https = require('https');

const server = http.createServer((req, res) => {
    console.info(req.url);
    let url = getUrl(req.url);
    if (!url) {
        res.statusCode = 404;
        res.end('use: /---http://... or ?url=http://...');
        return;
    }
    console.info(url.href);
    let h = https;
    if (url.protocol === "http:") h = http;
    const options = {
        method: req.method,
        headers: req.headers
    };
    options.headers.host = url.host;
    //console.info(options.headers);
    const proxyReq = h.request(url, options, (proxyRes) => {
        res.writeHead(proxyRes.statusCode, proxyRes.headers);
        proxyRes.pipe(res);
    });

    req.pipe(proxyReq);

    proxyReq.on('error', (error) => {
        console.error('Proxy request error:', error);
        res.statusCode = 500;
        res.end('Internal Server Error');
    });
});

const port = process.env.PORT || 10000;
server.listen(port, () => {
    console.log(`Server listening on port ${port}`);
});

function getUrl(u) {
    if (u.startsWith("/---")) {
        u = u.replace(/^\/-{3,}/, '');
        return new URL(u);
    }
    let s = new URLSearchParams(u.substring(1));
    let u2 = s.get("url") || "";
    if (u2) return new URL(u2);
    return null;
}
